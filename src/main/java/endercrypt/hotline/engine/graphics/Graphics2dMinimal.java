package endercrypt.hotline.engine.graphics;


import java.awt.Color;
import java.awt.Composite;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Paint;
import java.awt.RenderingHints.Key;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.geom.AffineTransform;
import java.util.Objects;


public class Graphics2dMinimal
{
	private final Graphics2D g2d;
	
	public Graphics2dMinimal(Graphics2D g2d)
	{
		Objects.requireNonNull(g2d, "g2d");
		this.g2d = g2d;
	}
	
	public Graphics2D getInternalGraphics2D()
	{
		return g2d;
	}
	
	// CREATE //
	
	public Graphics2D create()
	{
		return (Graphics2D) g2d.create();
	}
	
	public Graphics2D create(int x, int y, int width, int height)
	{
		return (Graphics2D) g2d.create(x, y, width, height);
	}
	
	// CLEAR //
	
	public void clearRect(int x, int y, int width, int height)
	{
		g2d.clearRect(x, y, width, height);
	}
	
	// IMAGE //
	
	public boolean drawImage(Image img, AffineTransform xform)
	{
		return g2d.drawImage(img, xform, null);
	}
	
	public boolean drawImage(Image img, int x, int y)
	{
		return g2d.drawImage(img, x, y, null);
	}
	
	// STRING //
	
	public void drawString(String str, int x, int y)
	{
		g2d.drawString(str, x, y);
	}
	
	public void drawString(String str, double x, double y)
	{
		g2d.drawString(str, (float) x, (float) y);
	}
	
	// SHAPE //
	
	public void draw(Shape s)
	{
		g2d.draw(s);
	}
	
	public void fill(Shape s)
	{
		g2d.fill(s);
	}
	
	// SHAPES //
	
	public void drawRoundRect(int x, int y, int width, int height, int arcWidth, int arcHeight)
	{
		g2d.drawRoundRect(x, y, width, height, arcWidth, arcHeight);
	}
	
	public void fillRoundRect(int x, int y, int width, int height, int arcWidth, int arcHeight)
	{
		g2d.fillRoundRect(x, y, width, height, arcWidth, arcHeight);
	}
	
	public void drawOval(int x, int y, int width, int height)
	{
		g2d.drawOval(x, y, width, height);
	}
	
	public void fillOval(int x, int y, int width, int height)
	{
		g2d.fillOval(x, y, width, height);
	}
	
	public void drawArc(int x, int y, int width, int height, int startAngle, int arcAngle)
	{
		g2d.drawArc(x, y, width, height, startAngle, arcAngle);
	}
	
	public void drawLine(int x1, int y1, int x2, int y2)
	{
		g2d.drawLine(x1, y1, x2, y2);
	}
	
	public void fillRect(int x, int y, int width, int height)
	{
		g2d.fillRect(x, y, width, height);
	}
	
	public void drawRect(int x, int y, int width, int height)
	{
		g2d.drawRect(x, y, width, height);
	}
	
	// RENDERING HINTS //
	
	public void setRenderingHint(Key hintKey, Object hintValue)
	{
		g2d.setRenderingHint(hintKey, hintValue);
	}
	
	// TRANSFORM //
	
	public void transform(AffineTransform Tx)
	{
		g2d.transform(Tx);
	}
	
	public void setTransform(AffineTransform Tx)
	{
		g2d.setTransform(Tx);
	}
	
	public AffineTransform getTransform()
	{
		return g2d.getTransform();
	}
	
	// COLOR //
	
	public void setColor(Color c)
	{
		g2d.setColor(c);
	}
	
	public Color getColor()
	{
		return g2d.getColor();
	}
	
	// FONT //
	
	public void setFont(Font font)
	{
		g2d.setFont(font);
	}
	
	public Font getFont()
	{
		return g2d.getFont();
	}
	
	// COMPOSITE //
	
	public void setComposite(Composite comp)
	{
		g2d.setComposite(comp);
	}
	
	public Composite getComposite()
	{
		return g2d.getComposite();
	}
	
	// PAINT //
	
	public void setPaint(Paint paint)
	{
		g2d.setPaint(paint);
	}
	
	public Paint getPaint()
	{
		return g2d.getPaint();
	}
	
	// STROKE //
	
	public void setStroke(Stroke s)
	{
		g2d.setStroke(s);
	}
	
	public Stroke getStroke()
	{
		return g2d.getStroke();
	}
	
	// MISC //
	
	public void setXORMode(Color c1)
	{
		g2d.setXORMode(c1);
	}
	
	public FontMetrics getFontMetrics()
	{
		return g2d.getFontMetrics();
	}
	
	public void dispose()
	{
		g2d.dispose();
	}
	
	// OVERRIDES //
	
	@Override
	public int hashCode()
	{
		return g2d.hashCode();
	}
	
	@Override
	public boolean equals(Object obj)
	{
		return g2d.equals(obj);
	}
	
	@Override
	public String toString()
	{
		return g2d.toString();
	}
}
