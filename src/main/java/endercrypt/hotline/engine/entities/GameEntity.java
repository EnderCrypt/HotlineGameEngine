package endercrypt.hotline.engine.entities;


import java.awt.Shape;
import java.awt.geom.Rectangle2D;
import java.io.Serializable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import endercrypt.hotline.engine.core.HotlineGameEngine;
import endercrypt.hotline.engine.event.Event;
import endercrypt.hotline.engine.event.EventListener;
import endercrypt.hotline.engine.event.GameEntityEventCollector;
import endercrypt.hotline.engine.event.GameEntityEventManager;
import endercrypt.hotline.engine.event.impl.mouse.EventMouse;
import endercrypt.hotline.engine.event.impl.mouse.GlobalMouseEvent;
import endercrypt.hotline.engine.event.impl.mouse.LocalMouseEvent;
import endercrypt.hotline.engine.graphics.HotlineGraphics;
import endercrypt.hotline.engine.mouse.MouseButton;
import endercrypt.hotline.engine.room.Room;
import endercrypt.hotline.engine.sprite.Sprite;
import endercrypt.hotline.engine.sprite.SpriteInfo;
import endercrypt.hotline.engine.sprite.SpriteUtility;
import endercrypt.hotline.engine.timer.TimerManager;
import endercrypt.library.position.Motion;
import endercrypt.library.position.Position;


public abstract class GameEntity implements Serializable
{
	private static final long serialVersionUID = -92455754043759309L;
	
	/**
	 * 
	 */
	
	private static final Logger logger = LogManager.getLogger(GameEntity.class);
	
	private Room room = null;
	private EntityState entityState = EntityState.UNATTACHED;
	
	private long frames = 0;
	public final TimerManager timers = new TimerManager(this);
	
	public Sprite sprite = null;
	public final SpriteInfo spriteInfo = new SpriteInfo();
	public int depth = 0;
	
	public final Position position = new Position(0.0, 0.0);
	public final Motion motion = new Motion(0.0, 0.0);
	
	public GameEntity()
	{
		
	}
	
	// ROOM HOOKS //
	
	final void attach(Room room)
	{
		if (entityState == EntityState.DEAD)
		{
			throw new EntityException("The entity " + this + " is dead");
		}
		if (entityState == EntityState.ATTACHED)
		{
			if (this.room == room)
			{
				throw new EntityException("The entity " + this + " is already attached to this room");
			}
			else
			{
				throw new EntityException("The entity " + this + " is already attached to room " + this.room);
			}
		}
		this.room = room;
		entityState = EntityState.ATTACHED;
		logger.debug(this + " is now attached to " + room);
		getEventManager().trigger(Event.CREATE);
	}
	
	// VARIABLES //
	
	public EntityState getEntityState()
	{
		return entityState;
	}
	
	public boolean isAlive()
	{
		return getEntityState() == EntityState.ATTACHED;
	}
	
	// STUFF //
	
	public Room getRoom()
	{
		if (room == null)
		{
			throw new IllegalStateException("room unavailable, state: " + getEntityState());
		}
		return room;
	}
	
	public HotlineGameEngine getHotline()
	{
		return getRoom().getHotline();
	}
	
	public GameEntityEventManager getEventManager()
	{
		return GameEntityEventCollector.resolve(this);
	}
	
	// METHODS //
	
	public long getFrames()
	{
		return frames;
	}
	
	public void destroy()
	{
		entityState = EntityState.DEAD;
	}
	
	// EVENTS //
	
	@EventListener(Event.UPDATE)
	private void gameEntityEventUpdate()
	{
		// frames
		frames++;
		
		// timer
		timers.update();
		
		// motion
		position.add(motion);
		
		// animation
		if (sprite == null)
		{
			spriteInfo.frame = 0;
		}
		else
		{
			spriteInfo.frame += spriteInfo.frame_speed;
			int totoalFrames = sprite.getSpriteFrames().countFrames();
			if (spriteInfo.frame > totoalFrames)
			{
				spriteInfo.frame -= totoalFrames;
			}
		}
	}
	
	@EventListener(Event.MOUSE)
	@EventMouse(global = true, button = MouseButton.ANY)
	private final void gameEntityEventGlobalMousePress(GlobalMouseEvent event)
	{
		Position mouse = getHotline().getMouse().getPositionInRoom();
		if (SpriteUtility.getRotatedSpriteRectangle(this).contains(mouse.x, mouse.y))
		{
			Position offset = new Position(mouse.x - position.x, mouse.y - position.y);
			getEventManager().trigger(Event.MOUSE, LocalMouseEvent.fromGlobal(event, this, offset));
		}
	}
	
	@EventListener(Event.DRAW)
	protected void onDraw(HotlineGraphics graphics)
	{
		drawSelf(graphics);
	}
	
	protected void drawSelf(HotlineGraphics graphics)
	{
		if (sprite != null)
		{
			sprite.draw(graphics, position, spriteInfo);
		}
	}
	
	// MISC //
	
	public boolean isCollidesWith(GameEntity other)
	{
		if (sprite != null && other.sprite != null)
		{
			Shape thisSprite = SpriteUtility.getRotatedSpriteRectangle(this, -other.spriteInfo.rotation);
			Rectangle2D otherSprite = SpriteUtility.getSpriteRectangle(other);
			return thisSprite.intersects(otherSprite);
		}
		return false;
	}
}
