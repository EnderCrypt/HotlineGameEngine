package endercrypt.hotline.engine.entities;

public enum EntityState
{
	UNATTACHED, ATTACHED, DEAD;
}
