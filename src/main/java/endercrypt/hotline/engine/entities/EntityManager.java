package endercrypt.hotline.engine.entities;


import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import endercrypt.hotline.engine.room.Room;
import endercrypt.library.position.Position;


public class EntityManager implements Serializable
{
	private static final long serialVersionUID = -4603230297232749703L;
	
	/**
	 * 
	 */
	
	private Map<Class<? extends GameEntity>, Set<? extends GameEntity>> entities = new HashMap<>();
	
	private Room room;
	
	public EntityManager(Room room)
	{
		this.room = room;
	}
	
	// ADD //
	
	public void add(GameEntity entity, Position position)
	{
		add(entity, position.x, position.y);
	}
	
	public synchronized void add(GameEntity entity, double x, double y)
	{
		entity.position.set(x, y);
		add(entity);
	}
	
	public synchronized void add(GameEntity entity)
	{
		entity.attach(room);
		getCollection(entity).add(entity);
	}
	
	public void addAll(GameEntity... entitiesArray)
	{
		Arrays.stream(entitiesArray).forEach(this::add);
	}
	
	public void addAll(Collection<GameEntity> entitiesCollection)
	{
		entitiesCollection.stream().forEach(this::add);
	}
	
	// GET //
	
	public synchronized <T extends GameEntity> Optional<T> getEntity(Class<T> entityClass)
	{
		return getCollection(entityClass).stream().findAny();
	}
	
	public synchronized <T extends GameEntity> Set<T> getEntitiesOf(Class<T> entityClass)
	{
		return Collections.unmodifiableSet(getCollection(entityClass));
	}
	
	@SuppressWarnings("unchecked")
	public synchronized <T extends GameEntity> List<T> getEntitiesOfSubtype(Class<T> entityClass)
	{
		Set<Class<? extends GameEntity>> classes = entities.keySet();
		classes.removeIf(c -> !entityClass.isAssignableFrom(c));
		List<? extends T> result = (List<? extends T>) classes.stream().flatMap(c -> getCollection(c).stream()).collect(Collectors.toList());
		return Collections.unmodifiableList(result);
	}
	
	public synchronized List<GameEntity> getAllEntities()
	{
		return entities.values().stream().flatMap(Collection::stream).collect(Collectors.toList());
	}
	
	public void forEach(Consumer<GameEntity> consumer)
	{
		getAllEntities().forEach(consumer::accept);
	}
	
	public Stream<GameEntity> getAllEntitiesByDepth()
	{
		return getAllEntities().stream().sorted((e1, e2) -> Integer.compare(e2.depth, e1.depth));
	}
	
	// COUNT //
	
	public synchronized int countEntities()
	{
		return entities.values().stream().mapToInt(Set::size).sum();
	}
	
	public synchronized int countEntities(Class<? extends GameEntity> entityClass)
	{
		return getCollection(entityClass).size();
	}
	
	// REMOVE //
	
	public void remove(GameEntity entity)
	{
		if (entity.getEntityState() == EntityState.ATTACHED)
		{
			entity.destroy();
		}
		getCollection(entity).remove(entity);
	}
	
	public void clear(Class<? extends GameEntity> entityClass)
	{
		getCollection(entityClass).forEach(this::remove);
	}
	
	public void clearAll()
	{
		for (Class<? extends GameEntity> entityClass : entities.keySet())
		{
			clear(entityClass);
		}
	}
	
	// MISC //
	
	public synchronized boolean contains(GameEntity entity)
	{
		return getCollection(entity).contains(entity);
	}
	
	// COLLECTION //
	
	@SuppressWarnings("unchecked")
	private synchronized <T extends GameEntity> Set<T> getCollection(T entity)
	{
		return (Set<T>) getCollection(entity.getClass());
	}
	
	@SuppressWarnings("unchecked")
	private synchronized <T extends GameEntity> Set<T> getCollection(Class<T> entityClass)
	{
		Set<T> list = (Set<T>) entities.get(entityClass);
		if (list == null)
		{
			list = new HashSet<>();
			entities.put(entityClass, list);
		}
		return list;
	}
}
