package endercrypt.hotline.engine.timer;


import java.io.Serializable;

import endercrypt.hotline.engine.entities.GameEntity;


public abstract class TimerInstance implements Comparable<TimerInstance>, Serializable
{
	private static final long serialVersionUID = 3314279794244268182L;
	
	/**
	 * 
	 */
	
	private long frame;
	
	public TimerInstance(long frame)
	{
		if (frame < 0)
		{
			throw new IllegalArgumentException("frame cannot be negative");
		}
		this.frame = frame;
	}
	
	public long getFrame()
	{
		return frame;
	}
	
	public final void call(GameEntity gameEntity)
	{
		try
		{
			internalCall(gameEntity);
		}
		catch (Exception e)
		{
			gameEntity.getHotline().crash(e);
		}
	}
	
	public abstract void internalCall(GameEntity gameEntity) throws Exception;
	
	@Override
	public int compareTo(TimerInstance other)
	{
		return Long.compare(frame, other.frame);
	}
}
