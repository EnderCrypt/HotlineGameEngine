package endercrypt.hotline.engine.timer.impl;


import java.util.Objects;

import endercrypt.hotline.engine.entities.GameEntity;
import endercrypt.hotline.engine.timer.TimerInstance;


public class TimerCallbackInstance extends TimerInstance
{
	private static final long serialVersionUID = 7561872498836352241L;
	
	/**
	 * 
	 */
	
	private TimerCallback callback;
	
	public TimerCallbackInstance(long frame, TimerCallback callback)
	{
		super(frame);
		Objects.requireNonNull(callback, "callback");
		this.callback = callback;
	}
	
	public TimerCallback getCallback()
	{
		return callback;
	}
	
	@Override
	public void internalCall(GameEntity gameEntity) throws Exception
	{
		callback.call();
	}
}
