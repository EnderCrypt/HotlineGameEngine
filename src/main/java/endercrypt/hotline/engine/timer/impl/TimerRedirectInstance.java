package endercrypt.hotline.engine.timer.impl;


import java.util.Objects;

import endercrypt.hotline.engine.entities.GameEntity;
import endercrypt.hotline.engine.event.Event;
import endercrypt.hotline.engine.timer.TimerInstance;


public class TimerRedirectInstance extends TimerInstance
{
	private static final long serialVersionUID = 7561872498836352241L;
	
	/**
	 * 
	 */
	
	private String name;
	private Object[] args;
	
	public TimerRedirectInstance(long frame, String name, Object... args)
	{
		super(frame);
		Objects.requireNonNull(name, "name");
		this.name = name;
		this.args = args;
	}
	
	public String getName()
	{
		return name;
	}
	
	public Object[] getArgs()
	{
		return args;
	}
	
	@Override
	public void internalCall(GameEntity gameEntity)
	{
		gameEntity.getEventManager().trigger(Event.TIMER, this);
	}
}
