package endercrypt.hotline.engine.timer.impl;


import java.io.Serializable;


public interface TimerCallback extends Serializable
{
	public void call() throws Exception;
}
