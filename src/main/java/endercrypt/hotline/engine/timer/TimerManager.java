package endercrypt.hotline.engine.timer;


import java.io.Serializable;
import java.util.PriorityQueue;

import endercrypt.hotline.engine.entities.GameEntity;
import endercrypt.hotline.engine.timer.impl.TimerCallback;
import endercrypt.hotline.engine.timer.impl.TimerCallbackInstance;
import endercrypt.hotline.engine.timer.impl.TimerRedirectInstance;


public class TimerManager implements Serializable
{
	private static final long serialVersionUID = -6349610401060824575L;
	
	/**
	 * 
	 */
	
	private GameEntity gameEntity;
	private PriorityQueue<TimerInstance> timers = new PriorityQueue<>();
	
	public TimerManager(GameEntity gameEntity)
	{
		this.gameEntity = gameEntity;
	}
	
	public void forwardTimer(long frame, TimerCallback callback)
	{
		if (frame <= 0)
		{
			throw new IllegalArgumentException("frame should be higher than 0");
		}
		setTimer((gameEntity.getFrames() + frame), callback);
	}
	
	public void forwardTimer(long frame, String name, Object... args)
	{
		if (frame <= 0)
		{
			throw new IllegalArgumentException("frame should be higher than 0");
		}
		setTimer((gameEntity.getFrames() + frame), name, args);
	}
	
	public void setTimer(long frame, TimerCallback callback)
	{
		setTimer(new TimerCallbackInstance(frame, callback));
	}
	
	public void setTimer(long frame, String name, Object... args)
	{
		setTimer(new TimerRedirectInstance(frame, name, args));
	}
	
	private void setTimer(TimerInstance timerInstance)
	{
		long frame = timerInstance.getFrame();
		if (frame <= gameEntity.getFrames())
		{
			throw new IllegalArgumentException("frame " + frame + " has already occured");
		}
		timers.add(timerInstance);
	}
	
	public void update()
	{
		long frame = gameEntity.getFrames();
		while (true)
		{
			TimerInstance timerInstance = timers.peek();
			if (timerInstance == null)
			{
				break;
			}
			if (timerInstance.getFrame() < frame)
			{
				throw new TimerException("missed timer from " + timerInstance.getFrame() + " (current frame: " + frame + ")");
			}
			if (timerInstance.getFrame() > frame)
			{
				break;
			}
			if (timerInstance.getFrame() == frame)
			{
				timers.remove();
				timerInstance.call(gameEntity);
			}
		}
	}
}
