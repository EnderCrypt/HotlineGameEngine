package endercrypt.hotline.engine.utility;


import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.management.ManagementFactory;
import java.nio.file.Path;

import endercrypt.hotline.engine.room.Room;


public class HotlineUtility
{
	public static final boolean eclipseDebugMode = ManagementFactory.getRuntimeMXBean().getInputArguments().toString().indexOf("-agentlib:jdwp") >= 0;
	
	private HotlineUtility()
	{
		throw new RuntimeException();
	}
	
	public static int round(double value)
	{
		return (int) round(value, 0);
	}
	
	public static double round(double value, int decimals)
	{
		double mult = Math.pow(10, decimals);
		return Math.round(value * mult) / mult;
	}
	
	public static void writeRoomSave(Path path, Room room) throws FileNotFoundException, IOException
	{
		try (ObjectOutputStream output = new ObjectOutputStream(new FileOutputStream(path.toFile())))
		{
			output.writeObject(room);
		}
	}
	
	public static Room readRoomSave(Path path) throws FileNotFoundException, IOException, ClassNotFoundException
	{
		try (ObjectInputStream input = new ObjectInputStream(new FileInputStream(path.toFile())))
		{
			return (Room) input.readObject();
		}
	}
}
