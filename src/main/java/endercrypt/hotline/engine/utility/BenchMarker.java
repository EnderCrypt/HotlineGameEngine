package endercrypt.hotline.engine.utility;


import endercrypt.hotline.engine.utility.timestring.TimeString;


public class BenchMarker
{
	private static long getRealTime()
	{
		return System.currentTimeMillis();
	}
	
	private long start;
	
	public BenchMarker()
	{
		start = getRealTime();
	}
	
	public long getTime()
	{
		return (getRealTime() - start);
	}
	
	public TimeString getAsTimeString()
	{
		return new TimeString(getTime());
	}
}
