package endercrypt.hotline.engine.utility.timestring;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


public class TimePiece
{
	private static List<TimePiece> pieces = new ArrayList<>();
	
	public static final TimePiece MILLISECOND = new TimePiece("millisecond", 1);
	public static final TimePiece SECOND = new TimePiece("second", MILLISECOND.getTime() * 1000);
	public static final TimePiece MINUTE = new TimePiece("minute", SECOND.getTime() * 60);
	public static final TimePiece HOUR = new TimePiece("hour", MINUTE.getTime() * 60);
	public static final TimePiece DAY = new TimePiece("day", HOUR.getTime() * 24);
	public static final TimePiece MONTH = new TimePiece("month", (long) (DAY.getTime() * 365.2422 / 12));
	public static final TimePiece YEAR = new TimePiece("year", (long) (DAY.getTime() * 365.2422));
	
	static
	{
		pieces.sort(new Comparator<TimePiece>()
		{
			@Override
			public int compare(TimePiece t1, TimePiece t2)
			{
				return Long.compare(t1.getTime(), t2.getTime());
			}
		});
	}
	
	public static List<TimePiece> getValues()
	{
		return Collections.unmodifiableList(pieces);
	}
	
	private String name;
	private long time;
	
	private TimePiece(String name, long time)
	{
		this.name = name;
		this.time = time;
		pieces.add(this);
	}
	
	public long getTime()
	{
		return this.time;
	}
	
	public String getName()
	{
		return this.name;
	}
}
