package endercrypt.hotline.engine.utility.timestring;

public class TimeLength
{
	public TimePiece timePiece;
	public long count;

	public TimeLength(TimePiece timePiece, long count)
	{
		this.timePiece = timePiece;
		this.count = count;
	}

	public TimePiece getTimePiece()
	{
		return this.timePiece;
	}

	public long getCount()
	{
		return this.count;
	}

	@Override
	public String toString()
	{
		String name = this.timePiece.getName();
		if (getCount() > 1)
		{
			name = name + "s";
		}
		return getCount() + " " + name;
	}
}