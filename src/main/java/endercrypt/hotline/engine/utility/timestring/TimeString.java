package endercrypt.hotline.engine.utility.timestring;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;


public class TimeString
{
	private boolean negative = false;
	private List<TimeLength> lengths = new ArrayList<>();
	
	public TimeString(long time)
	{
		if (time < 0)
		{
			this.negative = true;
			time = -time;
		}
		List<TimePiece> timePieces = TimePiece.getValues();
		for (int i = timePieces.size() - 1; i >= 0; i--)
		{
			TimePiece timePiece = timePieces.get(i);
			long timeLength = timePiece.getTime();
			long count = time / timeLength;
			time -= count * timeLength;
			TimeLength timeLengthObject = new TimeLength(timePiece, count);
			this.lengths.add(0, timeLengthObject);
		}
	}
	
	public boolean isNegative()
	{
		return this.negative;
	}
	
	public List<TimeLength> getHighest(int count)
	{
		List<TimeLength> results = new ArrayList<>();
		for (int i = this.lengths.size() - 1; i >= 0; i--)
		{
			TimeLength timeLength = this.lengths.get(i);
			if (timeLength.getCount() > 0)
			{
				results.add(timeLength);
				if (results.size() >= count)
				{
					break;
				}
			}
		}
		return results;
	}
	
	public String fullTimeString()
	{
		return getHighestToString(getTimeLengthCount());
	}
	
	public String getHighestToString(int count)
	{
		StringBuilder sb = new StringBuilder();
		Iterator<TimeLength> iterator = getHighest(count).iterator();
		if (isNegative())
		{
			sb.append("negative");
			if (iterator.hasNext())
			{
				sb.append(" ");
			}
		}
		if (iterator.hasNext() == false)
		{
			sb.append("no time");
		}
		while (iterator.hasNext())
		{
			TimeLength timeLength = iterator.next();
			sb.append(timeLength.getCount());
			sb.append(" ");
			sb.append(timeLength.getTimePiece().getName());
			if (timeLength.getCount() > 1)
			{
				sb.append("s");
			}
			if (iterator.hasNext())
			{
				sb.append(" and ");
			}
		}
		return sb.toString();
	}
	
	public Optional<TimeLength> getHighest()
	{
		return getHighest(1).stream().findFirst();
	}
	
	public TimeLength get(TimePiece timePiece)
	{
		return this.lengths.stream().filter(t -> t.getTimePiece() == timePiece).findFirst().get();
	}
	
	public TimeLength get(int index)
	{
		return this.lengths.get(index);
	}
	
	public List<TimeLength> getTimeLengths()
	{
		return Collections.unmodifiableList(this.lengths);
	}
	
	public int getTimeLengthCount()
	{
		return this.lengths.size();
	}
	
	@Override
	public String toString()
	{
		return getClass().getSimpleName() + "[" + this.lengths.toString() + "]";
	}
}
