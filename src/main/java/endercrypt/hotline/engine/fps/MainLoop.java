package endercrypt.hotline.engine.fps;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import endercrypt.hotline.engine.core.HotlineGameEngine;
import endercrypt.hotline.engine.room.Room;
import endercrypt.hotline.engine.utility.HotlineUtility;


public class MainLoop
{
	private static final Logger logger = LogManager.getLogger(MainLoop.class);
	
	private final HotlineGameEngine hotline;
	
	private Thread thread;
	private long frame = 0;
	private int fpsTarget = 60;
	private List<Long> frames = new ArrayList<>();
	
	public MainLoop(HotlineGameEngine hotline)
	{
		this.hotline = hotline;
		thread = new Thread(new FpsThread(), "Main loop thread");
	}
	
	public Thread getThread()
	{
		return thread;
	}
	
	public long getFrame()
	{
		return frame;
	}
	
	public void setFpsTarget(int fps)
	{
		this.fpsTarget = fps;
	}
	
	public int getFpsTarget()
	{
		return fpsTarget;
	}
	
	public int estimateFps()
	{
		pruneFrames();
		synchronized (frames)
		{
			return frames.size();
		}
	}
	
	private void pruneFrames()
	{
		long now = System.currentTimeMillis();
		synchronized (frames)
		{
			Iterator<Long> iterator = frames.iterator();
			while (iterator.hasNext())
			{
				long time = iterator.next();
				if (time < now - 1000)
				{
					iterator.remove();
				}
				else
				{
					break;
				}
			}
		}
	}
	
	private void registerFrame()
	{
		synchronized (frames)
		{
			frames.add(System.currentTimeMillis());
		}
		pruneFrames();
	}
	
	public void shutdown()
	{
		thread.interrupt();
	}
	
	private class FpsThread implements Runnable
	{
		@Override
		public void run()
		{
			try
			{
				double next = System.currentTimeMillis();
				while (Thread.currentThread().isInterrupted() == false)
				{
					// frame
					frame++;
					
					// fps math
					double frameDelta = 1000.0 / fpsTarget;
					
					// sleep
					if (System.currentTimeMillis() >= next)
					{
						double behind = (System.currentTimeMillis() - next);
						int skippedFrames = (int) (behind / frameDelta) + 1;
						logger.warn("falling behind by " + HotlineUtility.round(behind, 2) + " ms (delta: " + HotlineUtility.round(frameDelta, 2) + " ms), missed " + skippedFrames + " frame" + (skippedFrames == 1 ? "" : "s"));
						next += frameDelta * skippedFrames;
					}
					else
					{
						while (System.currentTimeMillis() < next)
						{
							// wait
						}
					}
					
					// calculate fps
					next = next + frameDelta;
					
					// frame
					frame();
				}
			}
			catch (Exception e)
			{
				hotline.crash(e);
			}
		}
		
		private void frame()
		{
			// draw room
			hotline.getWindow().redraw();
			
			// keyboard
			hotline.getKeyboard().triggerHeldKeys();
			
			// mouse
			hotline.getMouse().update();
			
			// update room
			hotline.getRoomManager().getRoom().ifPresent(Room::update);
			
			// done
			registerFrame();
		}
	}
}
