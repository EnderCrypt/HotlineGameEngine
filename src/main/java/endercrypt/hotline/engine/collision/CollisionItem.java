package endercrypt.hotline.engine.collision;


import java.util.Arrays;
import java.util.Collections;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import endercrypt.hotline.engine.entities.GameEntity;
import endercrypt.hotline.engine.event.Event;
import endercrypt.hotline.engine.event.GameEntityEventCollector;
import endercrypt.hotline.engine.event.GameEntityEventManager;
import endercrypt.hotline.engine.event.impl.collision.SimpleCollisionEvent;


public class CollisionItem
{
	private final GameEntity entity;
	private final GameEntityEventManager eventManager;
	
	private final Set<Class<? extends GameEntity>> targets;
	
	public CollisionItem(GameEntity entity)
	{
		this.entity = entity;
		this.eventManager = entity.getEventManager();
		
		this.targets = GameEntityEventCollector
			.resolve(entity.getClass())
			.getEventList(Event.COLLISION)
			.stream()
			.map(e -> (SimpleCollisionEvent) e)
			.map(SimpleCollisionEvent::getTargetClasses)
			.flatMap(Arrays::stream)
			.collect(Collectors.toSet());
	}
	
	public GameEntity getEntity()
	{
		return entity;
	}
	
	public Set<Class<? extends GameEntity>> getTargets()
	{
		return Collections.unmodifiableSet(targets);
	}
	
	public boolean isAllowedCollisionWith(CollisionItem item)
	{
		return getTargets().contains(item.getEntity().getClass());
	}
	
	public void fireCollisionEvent(GameEntity other)
	{
		Objects.requireNonNull(other, "other");
		eventManager.trigger(Event.COLLISION, other);
	}
}
