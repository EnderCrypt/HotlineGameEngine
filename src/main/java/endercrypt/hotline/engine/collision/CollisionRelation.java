package endercrypt.hotline.engine.collision;


import endercrypt.hotline.engine.entities.GameEntity;


public class CollisionRelation
{
	private final CollisionItem first;
	private final CollisionItem second;
	private final boolean collides;
	
	public CollisionRelation(CollisionItem first, CollisionItem second)
	{
		this.first = first;
		this.second = second;
		this.collides = first.isAllowedCollisionWith(second) || second.isAllowedCollisionWith(first);
	}
	
	public CollisionItem getFirst()
	{
		return first;
	}
	
	public GameEntity getFirstEntity()
	{
		return getFirst().getEntity();
	}
	
	public CollisionItem getSecond()
	{
		return second;
	}
	
	public GameEntity getSecondEntity()
	{
		return getSecond().getEntity();
	}
	
	public boolean isCollides()
	{
		return collides;
	}
	
	public void commit()
	{
		if (isCollides() && getFirstEntity().isCollidesWith(getSecondEntity()))
		{
			getFirst().fireCollisionEvent(getSecondEntity());
			getSecond().fireCollisionEvent(getFirstEntity());
		}
	}
}
