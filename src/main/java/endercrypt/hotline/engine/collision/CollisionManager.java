package endercrypt.hotline.engine.collision;


import java.util.List;

import endercrypt.hotline.engine.entities.EntityManager;
import endercrypt.hotline.engine.entities.GameEntity;


public class CollisionManager
{
	public static void perform(EntityManager entityManager)
	{
		CollisionManager collisionManager = new CollisionManager(entityManager.getAllEntities());
		collisionManager.commit();
	}
	
	private final CollisionArray[] array;
	
	public CollisionManager(List<GameEntity> entitiesList)
	{
		CollisionItem[] entities = entitiesList.stream().map(CollisionItem::new).toArray(CollisionItem[]::new);
		array = new CollisionArray[entities.length];
		for (int i = 0; i < entities.length; i++)
		{
			array[i] = new CollisionArray(entities, i);
		}
	}
	
	private void commit()
	{
		for (int i = 0; i < array.length; i++)
		{
			array[i].commit();
		}
	}
}
