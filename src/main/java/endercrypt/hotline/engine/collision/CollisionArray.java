package endercrypt.hotline.engine.collision;

public class CollisionArray
{
	private final CollisionItem item;
	private final CollisionRelation[] others;
	
	public CollisionArray(CollisionItem[] entities, int index)
	{
		this.item = entities[index];
		this.others = new CollisionRelation[index];
		for (int i = 0; i < others.length; i++)
		{
			others[i] = new CollisionRelation(item, entities[i]);
		}
	}
	
	public CollisionItem getItem()
	{
		return item;
	}
	
	public void commit()
	{
		for (int i = 0; i < others.length; i++)
		{
			others[i].commit();
		}
	}
}
