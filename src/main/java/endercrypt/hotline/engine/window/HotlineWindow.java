package endercrypt.hotline.engine.window;


import java.awt.Dimension;

import javax.swing.JFrame;

import endercrypt.hotline.engine.core.HotlineGameEngine;
import endercrypt.library.position.Position;


public class HotlineWindow
{
	private final JFrame frame;
	private final HotlinePanel panel;
	private Position position = new Position();
	
	public HotlineWindow(HotlineGameEngine hotline)
	{
		panel = new HotlinePanel(hotline);
		hotline.getMouse().install(panel);
		
		frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.add(panel);
		frame.pack();
		setSize(new Dimension(1000, 500));
		frame.setVisible(true);
	}
	
	public JFrame getFrame()
	{
		return frame;
	}
	
	public void setTitle(String title)
	{
		frame.setTitle(title);
	}
	
	public void setSize(Dimension dimension)
	{
		frame.setSize(dimension);
		frame.setLocationRelativeTo(null);
	}
	
	public Position getMousePosition()
	{
		return position;
	}
	
	public Dimension getDisplaySize()
	{
		return panel.getSize();
	}
	
	public void redraw()
	{
		panel.repaint();
	}
	
	public void shutdown()
	{
		frame.dispose();
	}
}
