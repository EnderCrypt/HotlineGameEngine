package endercrypt.hotline.engine.event;


import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import endercrypt.hotline.engine.entities.GameEntity;


public class GameEntityEventCollection
{
	private Class<? extends GameEntity> gameEntityClass;
	
	private Map<Event, List<SimpleEvent>> events = new HashMap<>();
	
	public GameEntityEventCollection(Class<? extends GameEntity> gameEntityClass)
	{
		this.gameEntityClass = gameEntityClass;
		
		Class<?> clazz = gameEntityClass;
		while (clazz != null)
		{
			for (Method method : clazz.getDeclaredMethods())
			{
				SimpleEvent.construct(method).ifPresent(event -> getEventList(event.getEvent()).add(event));
			}
			clazz = clazz.getSuperclass();
		}
	}
	
	public List<SimpleEvent> getEventList(Event event)
	{
		return events.computeIfAbsent(event, (e) -> new ArrayList<>());
	}
	
	public Class<? extends GameEntity> getGameEntityClass()
	{
		return gameEntityClass;
	}
	
	public void trigger(GameEntity entity, Event event, Object... args)
	{
		try
		{
			getEventList(event).forEach((ei) -> ei.trigger(entity, args));
		}
		catch (InternalEventException e)
		{
			entity.getHotline().crash(e.getCause());
		}
	}
}
