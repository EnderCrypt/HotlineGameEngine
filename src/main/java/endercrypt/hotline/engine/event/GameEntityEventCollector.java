package endercrypt.hotline.engine.event;


import java.util.HashMap;
import java.util.Map;

import endercrypt.hotline.engine.entities.GameEntity;


public class GameEntityEventCollector
{
	private static Map<Class<? extends GameEntity>, GameEntityEventCollection> entityClasses = new HashMap<>();
	
	private GameEntityEventCollector()
	{
		throw new RuntimeException();
	}
	
	public static synchronized GameEntityEventManager resolve(GameEntity entity)
	{
		return new GameEntityEventManager(entity, resolve(entity.getClass()));
	}
	
	public static synchronized GameEntityEventCollection resolve(Class<? extends GameEntity> gameEntityClass)
	{
		return entityClasses.computeIfAbsent(gameEntityClass, GameEntityEventCollection::new);
	}
}
