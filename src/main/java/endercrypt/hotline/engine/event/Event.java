package endercrypt.hotline.engine.event;


import java.lang.reflect.Method;
import java.util.function.Function;

import endercrypt.hotline.engine.event.impl.collision.SimpleCollisionEvent;
import endercrypt.hotline.engine.event.impl.keyboard.SimpleKeyEvent;
import endercrypt.hotline.engine.event.impl.mouse.SimpleMouseEvent;
import endercrypt.hotline.engine.event.impl.timer.SimpleTimerEvent;


public enum Event
{
	CREATE(SimpleEvent::new),
	COLLISION(SimpleCollisionEvent::new),
	UPDATE(SimpleEvent::new),
	DRAW(SimpleEvent::new),
	DRAW_HUD(SimpleEvent::new),
	TIMER(SimpleTimerEvent::new),
	MOUSE(SimpleMouseEvent::new),
	KEY(SimpleKeyEvent::new),
	DESTROY(SimpleEvent::new),
	GAME_LOADED(SimpleEvent::new),
	GAME_SAVED(SimpleEvent::new);
	
	private final Function<Method, SimpleEvent> creator;
	
	private Event(Function<Method, SimpleEvent> creator)
	{
		this.creator = creator;
	}
	
	public SimpleEvent createEventInstance(Method method)
	{
		return creator.apply(method);
	}
}
