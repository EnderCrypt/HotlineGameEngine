package endercrypt.hotline.engine.event;


import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import endercrypt.hotline.engine.entities.GameEntity;


public class SimpleEvent
{
	private static final Logger logger = LogManager.getLogger(SimpleEvent.class);
	
	@SuppressWarnings("unchecked")
	protected static <T> T getArgument(Class<T> argumentClass, Object... args)
	{
		for (Object arg : args)
		{
			if (argumentClass.isInstance(arg))
			{
				return (T) arg;
			}
		}
		return null;
	}
	
	public static Optional<SimpleEvent> construct(Method method)
	{
		EventListener eventListener = method.getAnnotation(EventListener.class);
		if (eventListener == null)
		{
			return Optional.empty();
		}
		Event event = eventListener.value();
		return Optional.of(event.createEventInstance(method));
	}
	
	protected final Method method;
	protected final Event event;
	
	public SimpleEvent(Method method)
	{
		this.method = method;
		EventListener eventListener = method.getAnnotation(EventListener.class);
		if (eventListener == null)
		{
			throw new IllegalArgumentException(method + " does not have " + EventListener.class.getSimpleName());
		}
		event = eventListener.value();
		logger.debug("detected " + event + " for " + method);
		
		if (Modifier.isPublic(method.getModifiers()) == false)
		{
			method.setAccessible(true);
			logger.debug("forced method into being runtime public");
			// throw new EventException(method + " must be public");
		}
	}
	
	public Event getEvent()
	{
		return event;
	}
	
	public void trigger(GameEntity gameEntity, Object... args)
	{
		Class<?>[] parameterClasses = method.getParameterTypes();
		Object[] parameters = new Object[parameterClasses.length];
		for (int i = 0; i < parameterClasses.length; i++)
		{
			Class<?> parameterClass = parameterClasses[i];
			parameters[i] = getArgument(parameterClass, args);
		}
		call(gameEntity, parameters);
	}
	
	public void call(GameEntity gameEntity, Object... args)
	{
		try
		{
			method.invoke(gameEntity, args);
		}
		catch (IllegalAccessException e)
		{
			throw new EventException("bad access modifier on " + method);
		}
		catch (IllegalArgumentException e)
		{
			throw new EventException("attempted to call" + method + " with unsuitable arguments: " + Arrays.toString(args));
		}
		catch (InvocationTargetException e)
		{
			throw new InternalEventException(e.getCause());
		}
	}
}
