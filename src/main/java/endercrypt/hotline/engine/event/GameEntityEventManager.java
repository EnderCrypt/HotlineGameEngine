package endercrypt.hotline.engine.event;


import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import endercrypt.hotline.engine.entities.GameEntity;


public class GameEntityEventManager
{
	private static Executor executor = Executors.newFixedThreadPool(5);
	
	private final GameEntity entity;
	private final GameEntityEventCollection gameEntityEventCollection;
	
	public GameEntityEventManager(GameEntity entity, GameEntityEventCollection gameEntityEventCollection)
	{
		this.entity = entity;
		this.gameEntityEventCollection = gameEntityEventCollection;
	}
	
	public void triggerAsync(Event event, Object... args)
	{
		executor.execute(() -> {
			Thread.currentThread().setName("event thread (" + event + ")");
			trigger(event, args);
		});
	}
	
	public void trigger(Event event, Object... args)
	{
		gameEntityEventCollection.trigger(entity, event, args);
	}
}
