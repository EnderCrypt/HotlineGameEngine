package endercrypt.hotline.engine.event.impl.collision;


import java.lang.reflect.Method;
import java.util.Optional;

import endercrypt.hotline.engine.entities.GameEntity;
import endercrypt.hotline.engine.event.SimpleEvent;


public class SimpleCollisionEvent extends SimpleEvent
{
	private Class<? extends GameEntity>[] targetClasses;
	
	@SuppressWarnings("unchecked")
	public SimpleCollisionEvent(Method method)
	{
		super(method);
		targetClasses = Optional.ofNullable(method.getAnnotation(EventCollision.class))
			.map(EventCollision::value)
			.orElseGet(() -> new Class[0]);
	}
	
	public Class<? extends GameEntity>[] getTargetClasses()
	{
		return targetClasses;
	}
	
	@Override
	public void trigger(GameEntity gameEntity, Object... args)
	{
		for (Class<? extends GameEntity> targetClass : getTargetClasses())
		{
			GameEntity other = getArgument(targetClass, args);
			if (other != null)
			{
				super.trigger(gameEntity, args);
				return;
			}
		}
	}
}
