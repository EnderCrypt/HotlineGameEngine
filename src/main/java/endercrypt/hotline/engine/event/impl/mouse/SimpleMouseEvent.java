package endercrypt.hotline.engine.event.impl.mouse;


import java.lang.reflect.Method;

import endercrypt.hotline.engine.entities.GameEntity;
import endercrypt.hotline.engine.event.SimpleEvent;
import endercrypt.hotline.engine.mouse.MouseAction;
import endercrypt.hotline.engine.mouse.MouseButton;


public class SimpleMouseEvent extends SimpleEvent
{
	private MouseButton button = MouseButton.LEFT;
	private MouseAction action = MouseAction.PRESS;
	private boolean global = true;
	
	public SimpleMouseEvent(Method method)
	{
		super(method);
		EventMouse eventMouse = method.getAnnotation(EventMouse.class);
		if (eventMouse != null)
		{
			button = eventMouse.button();
			action = eventMouse.action();
			global = eventMouse.global();
		}
	}
	
	public MouseButton getButton()
	{
		return button;
	}
	
	public MouseAction getAction()
	{
		return action;
	}
	
	public boolean isGlobal()
	{
		return global;
	}
	
	@Override
	public void trigger(GameEntity gameEntity, Object... args)
	{
		MouseEvent mouseEvent = getArgument(MouseEvent.class, args);
		
		if (getButton() != MouseButton.ANY && mouseEvent.getButton().equals(getButton()) == false)
		{
			return;
		}
		if (mouseEvent.getAction().equals(getAction()) == false)
		{
			return;
		}
		if (mouseEvent.isGlobal() != isGlobal())
		{
			return;
		}
		
		super.trigger(gameEntity, args);
	}
}
