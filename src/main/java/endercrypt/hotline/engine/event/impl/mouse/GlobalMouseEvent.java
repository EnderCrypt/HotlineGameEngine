package endercrypt.hotline.engine.event.impl.mouse;


import endercrypt.hotline.engine.mouse.MouseAction;
import endercrypt.hotline.engine.mouse.MouseButton;
import endercrypt.library.position.Position;


public class GlobalMouseEvent extends MouseEvent
{
	public GlobalMouseEvent(MouseButton button, MouseAction action, Position position, Position roomPosition)
	{
		super(button, action, position, roomPosition);
	}
	
	@Override
	public final boolean isGlobal()
	{
		return true;
	}
}
