package endercrypt.hotline.engine.event.impl.mouse;


import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import endercrypt.hotline.engine.mouse.MouseAction;
import endercrypt.hotline.engine.mouse.MouseButton;


@Retention(RUNTIME)
@Target(METHOD)
public @interface EventMouse
{
	MouseButton button() default MouseButton.LEFT;
	
	MouseAction action() default MouseAction.PRESS;
	
	boolean global() default true;
}
