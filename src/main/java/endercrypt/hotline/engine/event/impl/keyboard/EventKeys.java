package endercrypt.hotline.engine.event.impl.keyboard;


import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import net.ddns.endercrypt.library.keyboardmanager.BindType;


@Retention(RUNTIME)
@Target(METHOD)
public @interface EventKeys
{
	int key() default -1;
	
	BindType type() default BindType.HOLD;
	
	boolean shift() default false;
	
	boolean ctrl() default false;
	
	boolean alt() default false;
}
