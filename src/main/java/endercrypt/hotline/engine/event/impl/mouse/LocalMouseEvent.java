package endercrypt.hotline.engine.event.impl.mouse;


import endercrypt.hotline.engine.entities.GameEntity;
import endercrypt.hotline.engine.mouse.MouseAction;
import endercrypt.hotline.engine.mouse.MouseButton;
import endercrypt.library.position.Position;


public class LocalMouseEvent extends MouseEvent
{
	public static LocalMouseEvent fromGlobal(MouseEvent event, GameEntity entity, Position offset)
	{
		return new LocalMouseEvent(event.getButton(), event.getAction(), event.getPosition(), event.getPosition(), entity, offset);
	}
	
	private final GameEntity entity;
	private final Position offset;
	
	public LocalMouseEvent(MouseButton button, MouseAction action, Position position, Position roomPosition, GameEntity entity, Position offset)
	{
		super(button, action, position, roomPosition);
		this.entity = entity;
		this.offset = offset;
	}
	
	public GameEntity getEntity()
	{
		return entity;
	}
	
	public Position getOffset()
	{
		return offset;
	}
	
	@Override
	public final boolean isGlobal()
	{
		return false;
	}
}
