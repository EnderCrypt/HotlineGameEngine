package endercrypt.hotline.engine.event.impl.keyboard;


import java.lang.reflect.Method;
import java.util.Objects;

import endercrypt.hotline.engine.entities.GameEntity;
import endercrypt.hotline.engine.event.SimpleEvent;
import net.ddns.endercrypt.library.keyboardmanager.BindType;
import net.ddns.endercrypt.library.keyboardmanager.KeyboardEvent;


public class SimpleKeyEvent extends SimpleEvent
{
	private int key = -1;
	private BindType type = BindType.HOLD;
	private boolean shift = false;
	private boolean ctrl = false;
	private boolean alt = false;
	
	public SimpleKeyEvent(Method method)
	{
		super(method);
		EventKeys eventKeys = method.getAnnotation(EventKeys.class);
		if (eventKeys != null)
		{
			key = eventKeys.key();
			type = eventKeys.type();
			shift = eventKeys.shift();
			ctrl = eventKeys.ctrl();
			alt = eventKeys.alt();
		}
	}
	
	@Override
	public void trigger(GameEntity gameEntity, Object... args)
	{
		KeyboardEvent keyboardEvent = getArgument(KeyboardEvent.class, args);
		Objects.requireNonNull(keyboardEvent, "keyEvent");
		if (key != -1 && key != keyboardEvent.getKeyCode())
		{
			return;
		}
		if (type != keyboardEvent.getBindType())
		{
			return;
		}
		if (shift && keyboardEvent.isShiftHeld() == false)
		{
			return;
		}
		if (ctrl && keyboardEvent.isCtrlHeld() == false)
		{
			return;
		}
		if (alt && keyboardEvent.isAltHeld() == false)
		{
			return;
		}
		super.trigger(gameEntity, args);
	}
}
