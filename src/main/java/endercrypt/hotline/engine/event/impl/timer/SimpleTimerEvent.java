package endercrypt.hotline.engine.event.impl.timer;


import java.lang.reflect.Method;
import java.util.Objects;

import endercrypt.hotline.engine.entities.GameEntity;
import endercrypt.hotline.engine.event.SimpleEvent;
import endercrypt.hotline.engine.timer.impl.TimerRedirectInstance;


public class SimpleTimerEvent extends SimpleEvent
{
	private String name;
	
	public SimpleTimerEvent(Method method)
	{
		super(method);
		EventTimer eventTimer = method.getAnnotation(EventTimer.class);
		if (eventTimer != null)
		{
			name = eventTimer.value();
		}
	}
	
	@Override
	public void trigger(GameEntity gameEntity, Object... args)
	{
		TimerRedirectInstance timerRedirectInstance = getArgument(TimerRedirectInstance.class, args);
		Objects.requireNonNull(timerRedirectInstance, "timerRedirectInstance");
		if (name.equals(timerRedirectInstance.getName()) == false)
		{
			return;
		}
		super.trigger(gameEntity, timerRedirectInstance.getArgs());
	}
}
