package endercrypt.hotline.engine.event.impl.collision;


import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import endercrypt.hotline.engine.entities.GameEntity;


@Retention(RUNTIME)
@Target(METHOD)
public @interface EventCollision
{
	Class<? extends GameEntity>[] value();
}
