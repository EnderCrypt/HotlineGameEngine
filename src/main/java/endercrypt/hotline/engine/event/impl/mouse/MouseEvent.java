package endercrypt.hotline.engine.event.impl.mouse;


import endercrypt.hotline.engine.mouse.MouseAction;
import endercrypt.hotline.engine.mouse.MouseButton;
import endercrypt.library.position.Position;


public abstract class MouseEvent
{
	private final MouseButton button;
	private final MouseAction action;
	private final Position position;
	private final Position roomPosition;
	
	public MouseEvent(MouseButton button, MouseAction action, Position position, Position roomPosition)
	{
		this.button = button;
		this.action = action;
		this.position = position;
		this.roomPosition = roomPosition;
	}
	
	public MouseButton getButton()
	{
		return button;
	}
	
	public MouseAction getAction()
	{
		return action;
	}
	
	public Position getPosition()
	{
		return position;
	}
	
	public Position getPositionInRoom()
	{
		return roomPosition;
	}
	
	public abstract boolean isGlobal();
}
