package endercrypt.hotline.engine.event;

@SuppressWarnings("serial")
public class InternalEventException extends EventException
{
	public InternalEventException(Throwable cause)
	{
		super(cause);
	}
}
