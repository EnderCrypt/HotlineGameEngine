package endercrypt.hotline.engine.core;


import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Path;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import endercrypt.hotline.engine.event.Event;
import endercrypt.hotline.engine.fps.MainLoop;
import endercrypt.hotline.engine.mouse.MouseManager;
import endercrypt.hotline.engine.room.Room;
import endercrypt.hotline.engine.room.RoomException;
import endercrypt.hotline.engine.room.RoomManager;
import endercrypt.hotline.engine.room.impl.ExceptionRoom;
import endercrypt.hotline.engine.utility.BenchMarker;
import endercrypt.hotline.engine.utility.HotlineKeyboardListener;
import endercrypt.hotline.engine.utility.HotlineUtility;
import endercrypt.hotline.engine.window.HotlineWindow;
import net.ddns.endercrypt.library.keyboardmanager.KeyboardManager;
import net.ddns.endercrypt.library.keyboardmanager.binds.AnyKey;


public class HotlineGameEngine
{
	private static final Logger logger = LogManager.getLogger(HotlineGameEngine.class);
	
	private final RoomManager roomManager;
	
	private final MouseManager mouseManager;
	
	private final HotlineWindow window;
	
	private final KeyboardManager keyboard;
	
	private final MainLoop mainLoop;
	
	public HotlineGameEngine()
	{
		// room manager
		logger.info("Constructing hotline room manager");
		roomManager = new RoomManager(this);
		
		// mouse
		logger.info("Constructing hotline mouse");
		mouseManager = new MouseManager(this);
		
		// window
		logger.info("Constructing hotline window");
		window = new HotlineWindow(this);
		
		// keyboard listener
		logger.info("Constructing keyboard manager");
		keyboard = new KeyboardManager();
		logger.info("attaching main window to keyboard manager");
		keyboard.install(window.getFrame());
		keyboard.getListenerGroups().global().bind(new AnyKey(), new HotlineKeyboardListener(roomManager));
		
		// main loop
		logger.info("Constructing main loop");
		mainLoop = new MainLoop(this);
		logger.info("Starting main loop");
		mainLoop.getThread().start();
	}
	
	// getters
	public MouseManager getMouse()
	{
		return mouseManager;
	}
	
	public HotlineWindow getWindow()
	{
		return window;
	}
	
	public KeyboardManager getKeyboard()
	{
		return keyboard;
	}
	
	public RoomManager getRoomManager()
	{
		return roomManager;
	}
	
	public MainLoop getMainLoop()
	{
		return mainLoop;
	}
	
	// events
	public void fireEntityEvents(Event event, Object... args)
	{
		getRoomManager().getRoom().ifPresent(room -> room.fireEntityEvents(event, args));
	}
	
	// save & load
	public void save(Path path) throws FileNotFoundException, IOException
	{
		BenchMarker benchMarker = new BenchMarker();
		
		Room room = getRoomManager().getRoom()
			.orElseThrow(() -> new RoomException("cant save the game whitout a room present"));
		
		HotlineUtility.writeRoomSave(path, room);
		fireEntityEvents(Event.GAME_SAVED);
		
		logger.info("saved " + path + " (" + benchMarker.getAsTimeString().getHighestToString(2) + ")");
	}
	
	public void load(Path path) throws FileNotFoundException, IOException, ClassNotFoundException
	{
		BenchMarker benchMarker = new BenchMarker();
		
		Room room = HotlineUtility.readRoomSave(path);
		getRoomManager().setRoom(room);
		fireEntityEvents(Event.GAME_LOADED);
		
		logger.info("loaded " + path + " (" + benchMarker.getAsTimeString().getHighestToString(2) + ")");
	}
	
	public void crash(Throwable e)
	{
		logger.error(HotlineGameEngine.class.getSimpleName() + " crashed", e);
		getRoomManager().setRoom(new ExceptionRoom(e));
	}
	
	public void shutdown()
	{
		logger.info("Shutting down " + HotlineGameEngine.class.getSimpleName());
		getWindow().shutdown();
		getMainLoop().shutdown();
	}
}
