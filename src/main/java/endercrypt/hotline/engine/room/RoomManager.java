package endercrypt.hotline.engine.room;


import java.util.Optional;

import endercrypt.hotline.engine.core.HotlineGameEngine;


public class RoomManager
{
	private final HotlineGameEngine hotline;
	
	private Room room = null;
	
	public RoomManager(HotlineGameEngine hotline)
	{
		this.hotline = hotline;
	}
	
	public void setRoom(Room room)
	{
		room.attach(hotline);
		this.room = room;
	}
	
	public boolean hasRoom()
	{
		return (room != null);
	}
	
	public Optional<Room> getRoom()
	{
		return Optional.ofNullable(room);
	}
}
