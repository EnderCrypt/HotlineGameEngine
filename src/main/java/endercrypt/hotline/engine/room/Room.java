package endercrypt.hotline.engine.room;


import java.awt.Color;
import java.io.Serializable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import endercrypt.hotline.engine.collision.CollisionManager;
import endercrypt.hotline.engine.core.HotlineGameEngine;
import endercrypt.hotline.engine.entities.EntityManager;
import endercrypt.hotline.engine.entities.EntityState;
import endercrypt.hotline.engine.entities.GameEntity;
import endercrypt.hotline.engine.event.Event;
import endercrypt.hotline.engine.graphics.HotlineGraphics;
import endercrypt.library.position.Position;


public class Room implements Serializable
{
	private static final long serialVersionUID = -1442867546520862802L;
	
	/**
	 * 
	 */
	
	private static final Logger logger = LogManager.getLogger(Room.class);
	
	private transient HotlineGameEngine hotline;
	
	private EntityManager entities = new EntityManager(this);
	
	private View view = new View();
	
	private long frames = 0;
	
	final void attach(HotlineGameEngine hotline)
	{
		this.hotline = hotline;
	}
	
	public HotlineGameEngine getHotline()
	{
		return hotline;
	}
	
	public EntityManager getEntities()
	{
		return entities;
	}
	
	public View getView()
	{
		return view;
	}
	
	public long getFrames()
	{
		return frames;
	}
	
	public void fireEntityEvents(Event event, Object... args)
	{
		for (GameEntity gameEntity : entities.getAllEntities())
		{
			gameEntity.getEventManager().triggerAsync(event, args);
		}
	}
	
	public void update()
	{
		// frames
		frames++;
		
		// collision
		CollisionManager.perform(getEntities());
		
		// update entities
		fireEntityEvents(Event.UPDATE);
		
		// remove entities
		for (GameEntity gameEntity : entities.getAllEntities())
		{
			if (gameEntity.getEntityState() == EntityState.DEAD)
			{
				logger.debug("removing " + gameEntity + " from " + this);
				gameEntity.getEventManager().trigger(Event.DESTROY);
				getEntities().remove(gameEntity);
			}
		}
		
		// update view
		view.update();
	}
	
	public void draw(HotlineGraphics graphics)
	{
		// transform for view
		Position viewTranslation = getView().getTranslation(getHotline());
		graphics.getInternalGraphics2D().translate(viewTranslation.x, viewTranslation.y);
		
		// draw entities
		graphics.setColor(Color.BLACK);
		for (GameEntity gameEntity : entities.getAllEntitiesByDepth().toArray(GameEntity[]::new))
		{
			gameEntity.getEventManager().trigger(Event.DRAW, graphics);
		}
		
		// draw hud
		graphics.resetTransform();
		graphics.setColor(Color.BLACK);
		for (GameEntity gameEntity : entities.getAllEntitiesByDepth().toArray(GameEntity[]::new))
		{
			gameEntity.getEventManager().trigger(Event.DRAW_HUD, graphics);
		}
	}
}
