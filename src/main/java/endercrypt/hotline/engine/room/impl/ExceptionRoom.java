package endercrypt.hotline.engine.room.impl;


import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

import org.apache.commons.lang3.exception.ExceptionUtils;

import endercrypt.hotline.engine.core.HotlineGameEngine;
import endercrypt.hotline.engine.graphics.HotlineGraphics;
import endercrypt.hotline.engine.room.Room;


public class ExceptionRoom extends Room
{
	private static final long serialVersionUID = -5201288376975492113L;
	
	/**
	 * 
	 */
	
	public static final Font fontTitle = new Font("Monospaced", Font.PLAIN, 32);
	public static final Font fontMessage = new Font("Monospaced", Font.PLAIN, 16);
	
	private final String title;
	private final String message;
	
	public ExceptionRoom(Throwable e)
	{
		this(HotlineGameEngine.class.getSimpleName() + " crashed!", e);
	}
	
	public ExceptionRoom(String title, Throwable e)
	{
		this.title = title;
		this.message = ExceptionUtils.getStackTrace(e);
	}
	
	@Override
	public void draw(HotlineGraphics graphics)
	{
		super.draw(graphics);
		
		Dimension displaySize = graphics.getDisplaySize();
		
		graphics.clear(new Color(255, 50, 50));
		graphics.setColor(Color.BLACK);
		
		graphics.setFont(fontTitle);
		graphics.drawString(title, 10, 32);
		
		graphics.setFont(fontMessage);
		graphics.drawWrappedString(message, 10, 52, displaySize.width, displaySize.height);
	}
}
