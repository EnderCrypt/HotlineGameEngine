package endercrypt.hotline.engine.mouse;

public enum MouseAction
{
	RELEASE,
	HOLD,
	PRESS;
}
