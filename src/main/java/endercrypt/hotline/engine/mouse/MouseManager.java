package endercrypt.hotline.engine.mouse;


import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.Collections;
import java.util.EnumMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import endercrypt.hotline.engine.core.HotlineGameEngine;
import endercrypt.hotline.engine.event.Event;
import endercrypt.hotline.engine.event.impl.mouse.GlobalMouseEvent;
import endercrypt.hotline.engine.event.impl.mouse.MouseEvent;
import endercrypt.hotline.engine.window.HotlinePanel;
import endercrypt.library.position.Position;


public class MouseManager
{
	private static EnumMap<MouseButton, Boolean> emptyPressedMap()
	{
		EnumMap<MouseButton, Boolean> map = new EnumMap<>(MouseButton.class);
		for (MouseButton button : MouseButton.values())
		{
			map.put(button, false);
		}
		return map;
	}
	
	private final HotlineGameEngine hotline;
	private final InternalMouseListener mouseListener = new InternalMouseListener();
	
	private Position position = new Position();
	private EnumMap<MouseButton, Boolean> pressedPreviously = emptyPressedMap();
	private EnumMap<MouseButton, Boolean> pressed = emptyPressedMap();
	
	public MouseManager(HotlineGameEngine hotline)
	{
		this.hotline = hotline;
	}
	
	public void install(HotlinePanel panel)
	{
		panel.addMouseListener(mouseListener);
		panel.addMouseMotionListener(mouseListener);
	}
	
	public Position getPosition()
	{
		return Position.copy(position);
	}
	
	public Position getPositionInRoom()
	{
		final Position roomPosition = getPosition();
		
		hotline.getRoomManager().getRoom().ifPresent((room) -> {
			
			Position viewPosition = room.getView().getTranslation(hotline);
			roomPosition.add(-viewPosition.x, -viewPosition.y);
			
		});
		
		return roomPosition;
	}
	
	public List<MouseButton> getPressed()
	{
		synchronized (pressed)
		{
			return Collections.unmodifiableList(pressed.entrySet().stream().filter(Entry::getValue).map(Entry::getKey).collect(Collectors.toList()));
		}
	}
	
	private MouseEvent createMouseEvent(MouseButton button, MouseAction action)
	{
		return new GlobalMouseEvent(button, action, getPosition(), getPositionInRoom());
	}
	
	public void update()
	{
		synchronized (pressed)
		{
			for (Entry<MouseButton, Boolean> entry : pressed.entrySet())
			{
				MouseButton button = entry.getKey();
				boolean pressed = entry.getValue();
				if (pressed)
				{
					if (pressedPreviously.get(button) == false)
					{
						hotline.fireEntityEvents(Event.MOUSE, createMouseEvent(button, MouseAction.PRESS));
					}
					hotline.fireEntityEvents(Event.MOUSE, createMouseEvent(button, MouseAction.HOLD));
				}
				else
				{
					if (pressedPreviously.get(button) == true)
					{
						hotline.fireEntityEvents(Event.MOUSE, createMouseEvent(button, MouseAction.RELEASE));
					}
				}
				
				pressedPreviously.put(button, pressed);
			}
		}
	}
	
	private class InternalMouseListener implements MouseListener, MouseMotionListener
	{
		@Override
		public void mouseDragged(java.awt.event.MouseEvent e)
		{
			position.set(e.getX(), e.getY());
		}
		
		@Override
		public void mouseMoved(java.awt.event.MouseEvent e)
		{
			position.set(e.getX(), e.getY());
		}
		
		@Override
		public void mouseClicked(java.awt.event.MouseEvent e)
		{
			// nothing
		}
		
		@Override
		public void mousePressed(java.awt.event.MouseEvent e)
		{
			synchronized (pressed)
			{
				pressed.put(MouseButton.fromButton(e), true);
			}
		}
		
		@Override
		public void mouseReleased(java.awt.event.MouseEvent e)
		{
			synchronized (pressed)
			{
				pressed.put(MouseButton.fromButton(e), false);
			}
		}
		
		@Override
		public void mouseEntered(java.awt.event.MouseEvent e)
		{
			// nothing
		}
		
		@Override
		public void mouseExited(java.awt.event.MouseEvent e)
		{
			// nothing
		}
	}
}
