package endercrypt.hotline.engine.mouse;


import java.awt.event.MouseEvent;


public enum MouseButton
{
	LEFT,
	MIDDLE,
	RIGHT,
	ANY;
	
	public static final int BUTTONS = 3;
	
	public static MouseButton fromButton(MouseEvent mouseEvent)
	{
		return fromButton(mouseEvent.getButton());
	}
	
	public static MouseButton fromButton(int button)
	{
		if (button < 1 || button > 3)
		{
			throw new IllegalArgumentException("button cant be " + button);
		}
		return values()[button - 1];
	}
}
