package endercrypt.hotline.engine.sprite;


import java.awt.Dimension;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;

import endercrypt.hotline.engine.entities.GameEntity;
import endercrypt.hotline.engine.sprite.impl.PlainSpriteFrame;
import endercrypt.library.position.Position;


public class SpriteUtility
{
	private SpriteUtility()
	{
		throw new RuntimeException();
	}
	
	private static Position getSpriteRadius(PlainSpriteFrame sprite, SpriteInfo spriteInfo)
	{
		Dimension spriteDimension = sprite.getDimension();
		double x = ((spriteDimension.width * spriteInfo.scale_x) / 2.0);
		double y = ((spriteDimension.height * spriteInfo.scale_y) / 2.0);
		return new Position(x, y);
	}
	
	public static Rectangle2D getSpriteRectangle(GameEntity entity)
	{
		return getSpriteRectangle(entity.position, entity.sprite, entity.spriteInfo);
	}
	
	public static Rectangle2D getSpriteRectangle(Position position, Sprite sprite, SpriteInfo spriteInfo)
	{
		Position radius = getSpriteRadius(sprite.getSpriteFrames().getFrame(spriteInfo), spriteInfo);
		return new Rectangle2D.Double(position.x - radius.x, position.y - radius.y, radius.x * 2, radius.y * 2);
	}
	
	public static Shape getRotatedSpriteRectangle(GameEntity entity)
	{
		return getRotatedSpriteRectangle(entity.position, entity.sprite, entity.spriteInfo);
	}
	
	public static Shape getRotatedSpriteRectangle(Position position, Sprite sprite, SpriteInfo spriteInfo)
	{
		return getRotatedSpriteRectangle(position, sprite, spriteInfo, 0.0);
	}
	
	public static Shape getRotatedSpriteRectangle(GameEntity entity, double rotationOffse)
	{
		return getRotatedSpriteRectangle(entity.position, entity.sprite, entity.spriteInfo, rotationOffse);
	}
	
	public static Shape getRotatedSpriteRectangle(Position position, Sprite sprite, SpriteInfo spriteInfo, double rotationOffset)
	{
		double rotation = spriteInfo.rotation + rotationOffset;
		if (rotation % 90 <= 0.1)
		{
			return getSpriteRectangle(position, sprite, spriteInfo);
		}
		
		AffineTransform transform = new AffineTransform();
		transform.translate(position.x, position.y);
		transform.rotate(Math.toRadians(rotation), 0, 0);
		
		Rectangle2D rectangle = getSpriteRectangle(new Position(0.0, 0.0), sprite, spriteInfo);
		return transform.createTransformedShape(rectangle);
	}
	
}
