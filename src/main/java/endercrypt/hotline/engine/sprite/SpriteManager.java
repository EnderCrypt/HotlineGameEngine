package endercrypt.hotline.engine.sprite;


import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;

import javax.imageio.ImageIO;

import endercrypt.hotline.engine.sprite.impl.PlainSpriteFrame;


public class SpriteManager
{
	private static BufferedImage readImage(String path)
	{
		if (Files.exists(Paths.get(path)) == false)
		{
			throw new SpriteException("sprite " + path + " not found");
		}
		
		if (sprites.containsKey(path))
		{
			throw new SpriteException("Sprite already added");
		}
		
		try
		{
			return ImageIO.read(new File(path));
		}
		catch (IOException e)
		{
			throw new SpriteException("i/o error while reading: " + path, e);
		}
	}
	
	private static Function<BufferedImage, PlainSpriteFrame> spriteImageFactory = PlainSpriteFrame::new;
	
	private static boolean autoload = true;
	
	private static Map<String, SpriteFrameSequence> sprites = new HashMap<>();
	
	private SpriteManager()
	{
		throw new RuntimeException();
	}
	
	public static void setSpriteImageFactory(Function<BufferedImage, PlainSpriteFrame> spriteImageFactory)
	{
		Objects.requireNonNull(spriteImageFactory, "spriteImageFactory");
		SpriteManager.spriteImageFactory = spriteImageFactory;
	}
	
	public static boolean isAutoload()
	{
		return autoload;
	}
	
	public static void setAutoload(boolean autoload)
	{
		SpriteManager.autoload = autoload;
	}
	
	public static synchronized SpriteFrameSequence registerSprite(String path) throws SpriteException
	{
		BufferedImage image = readImage(path);
		return registerSprite(path, image, image.getWidth());
	}
	
	public static synchronized SpriteFrameSequence registerAnimatedSprite(String path, int frameWidth) throws SpriteException
	{
		BufferedImage image = readImage(path);
		return registerSprite(path, image, frameWidth);
	}
	
	private static synchronized SpriteFrameSequence registerSprite(String path, BufferedImage image, int frameWidth) throws SpriteException
	{
		List<PlainSpriteFrame> frames = new ArrayList<>();
		int x = 0;
		while (x < image.getWidth())
		{
			int legalWidth = Math.min(frameWidth, image.getWidth() - x);
			BufferedImage frame = image.getSubimage(x, 0, legalWidth, image.getHeight());
			PlainSpriteFrame spriteImage = spriteImageFactory.apply(frame);
			Objects.requireNonNull(spriteImage, "spriteImage");
			frames.add(spriteImage);
			
			x += frameWidth;
		}
		SpriteFrameSequence spriteImageSequence = new SpriteFrameSequence(frames);
		sprites.put(path, spriteImageSequence);
		return spriteImageSequence;
	}
	
	public static synchronized SpriteFrameSequence getSpriteImage(String path)
	{
		SpriteFrameSequence image = sprites.get(path);
		if (image == null)
		{
			if (autoload == false)
			{
				throw new SpriteException("Sprite not registered");
			}
			image = registerSprite(path);
		}
		return image;
	}
}
