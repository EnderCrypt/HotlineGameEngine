package endercrypt.hotline.engine.sprite;


import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.geom.AffineTransform;
import java.io.Serializable;

import endercrypt.hotline.engine.graphics.HotlineGraphics;
import endercrypt.hotline.engine.sprite.impl.PlainSpriteFrame;
import endercrypt.hotline.engine.utility.HotlineUtility;
import endercrypt.library.position.Position;


public class Sprite implements Serializable
{
	private static final long serialVersionUID = -4503798745690910893L;
	
	/**
	 * 
	 */
	
	public static Sprite of(String path)
	{
		return new Sprite(path);
	}
	
	private final String path;
	
	private transient SpriteFrameSequence spriteImage;
	
	private Sprite(String path)
	{
		this.path = path;
	}
	
	public SpriteFrameSequence getSpriteFrames()
	{
		if (spriteImage == null)
		{
			spriteImage = SpriteManager.getSpriteImage(getPath());
		}
		return spriteImage;
	}
	
	public String getPath()
	{
		return path;
	}
	
	public void draw(HotlineGraphics graphics, Position position, SpriteInfo spriteInfo)
	{
		// sprite frame
		PlainSpriteFrame frame = getSpriteFrames().getFrame(spriteInfo);
		
		// transform
		AffineTransform transform = spriteInfo.generateTransform(position, frame);
		
		// draw
		graphics.setComposite(AlphaComposite.SrcOver.derive((float) spriteInfo.alpha));
		graphics.drawImage(frame.getImage(), transform);
		
		// outline
		if (HotlineUtility.eclipseDebugMode)
		{
			graphics.setColor(Color.RED);
			graphics.draw(SpriteUtility.getRotatedSpriteRectangle(position, this, spriteInfo));
		}
	}
}
