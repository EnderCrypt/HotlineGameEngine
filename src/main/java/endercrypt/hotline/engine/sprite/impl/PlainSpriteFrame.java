package endercrypt.hotline.engine.sprite.impl;


import java.awt.Dimension;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.util.Objects;

import endercrypt.hotline.engine.sprite.SpriteException;
import endercrypt.library.position.Position;


public class PlainSpriteFrame
{
	protected final BufferedImage plainImage;
	
	private Position origin = new Position(0, 0);
	
	public PlainSpriteFrame(BufferedImage plainImage) throws SpriteException
	{
		Objects.requireNonNull(plainImage, "plainImage");
		this.plainImage = plainImage;
		
		setOriginCentered();
	}
	
	public Image getPlainImage()
	{
		return plainImage;
	}
	
	public Image getImage()
	{
		return getPlainImage();
	}
	
	public int getWidth()
	{
		return plainImage.getWidth(null);
	}
	
	public int getHeight()
	{
		return plainImage.getHeight(null);
	}
	
	public Dimension getDimension()
	{
		return new Dimension(getWidth(), getHeight());
	}
	
	public void setOriginCentered()
	{
		setOrigin(getWidth() / 2.0, getHeight() / 2.0);
	}
	
	public void setOrigin(double x, double y)
	{
		setOrigin(new Position(x, y));
	}
	
	public void setOrigin(Position origin)
	{
		this.origin = origin;
	}
	
	public Position getOrigin()
	{
		return origin;
	}
}
