package endercrypt.hotline.engine.sprite.impl;


import java.awt.Graphics2D;
import java.awt.Transparency;
import java.awt.image.BufferedImage;
import java.awt.image.VolatileImage;

import endercrypt.hotline.engine.sprite.SpriteException;


public class AcceleratedSpriteFrame extends SmartSpriteFrame<VolatileImage>
{
	static
	{
		System.setProperty("sun.java2d.opengl", "true");
	}
	
	public AcceleratedSpriteFrame(BufferedImage plainImage) throws SpriteException
	{
		super(plainImage);
	}
	
	@Override
	protected VolatileImage generateImage()
	{
		VolatileImage image = graphicsConfiguration.createCompatibleVolatileImage(getWidth(), getHeight(), Transparency.TRANSLUCENT);
		drawVolatileImage(image);
		return image;
	}
	
	private void drawVolatileImage(VolatileImage image)
	{
		Graphics2D g2d = image.createGraphics();
		g2d.drawImage(getPlainImage(), 0, 0, null);
		g2d.dispose();
	}
	
	@Override
	public VolatileImage getImage()
	{
		if (super.getImage().validate(graphicsConfiguration) == VolatileImage.IMAGE_INCOMPATIBLE)
		{
			resetImage();
		}
		else
		{
			if (super.getImage().contentsLost())
			{
				drawVolatileImage(super.getImage());
			}
		}
		return super.getImage();
	}
}
