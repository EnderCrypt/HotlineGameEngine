package endercrypt.hotline.engine.sprite.impl;


import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.util.Objects;

import endercrypt.hotline.engine.sprite.SpriteException;


public abstract class SmartSpriteFrame<T extends Image> extends PlainSpriteFrame
{
	protected static GraphicsEnvironment graphicsEnvironment = GraphicsEnvironment.getLocalGraphicsEnvironment();
	protected static GraphicsDevice graphicsDevice = graphicsEnvironment.getDefaultScreenDevice();
	protected static GraphicsConfiguration graphicsConfiguration = graphicsDevice.getDefaultConfiguration();
	
	private T image;
	
	public SmartSpriteFrame(BufferedImage plainImage) throws SpriteException
	{
		super(plainImage);
		resetImage();
	}
	
	protected void resetImage()
	{
		image = generateImage();
		Objects.requireNonNull(image, "image");
	}
	
	protected abstract T generateImage();
	
	@Override
	public T getImage()
	{
		Objects.requireNonNull(image, "image");
		return image;
	}
}
