package endercrypt.hotline.engine.sprite.impl;


import java.awt.Graphics2D;
import java.awt.Transparency;
import java.awt.image.BufferedImage;
import java.awt.image.VolatileImage;

import endercrypt.hotline.engine.sprite.SpriteException;


public class CompatibleSpriteImage extends SmartSpriteFrame<VolatileImage>
{
	public CompatibleSpriteImage(BufferedImage plainImage) throws SpriteException
	{
		super(plainImage);
	}
	
	@Override
	protected VolatileImage generateImage()
	{
		VolatileImage image = graphicsConfiguration.createCompatibleVolatileImage(getWidth(), getHeight(), Transparency.TRANSLUCENT);
		Graphics2D g2d = image.createGraphics();
		g2d.drawImage(getPlainImage(), 0, 0, null);
		g2d.dispose();
		return image;
	}
}
