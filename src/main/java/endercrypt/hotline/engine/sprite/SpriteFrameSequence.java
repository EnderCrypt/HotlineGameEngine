package endercrypt.hotline.engine.sprite;


import java.util.ArrayList;
import java.util.List;

import endercrypt.hotline.engine.sprite.impl.PlainSpriteFrame;


public class SpriteFrameSequence
{
	private List<PlainSpriteFrame> frames = new ArrayList<>();
	
	public SpriteFrameSequence(List<PlainSpriteFrame> frames)
	{
		if (frames.isEmpty())
		{
			throw new IllegalArgumentException("no sprite frames present");
		}
		this.frames.addAll(frames);
	}
	
	public int countFrames()
	{
		return frames.size();
	}
	
	public boolean isAnimated()
	{
		return countFrames() > 1;
	}
	
	public PlainSpriteFrame getFrame(SpriteInfo spriteInfo)
	{
		int index = Math.floorMod((int) spriteInfo.frame, countFrames());
		return getFrame(index);
	}
	
	public PlainSpriteFrame getFrame(int index)
	{
		return frames.get(index);
	}
}
