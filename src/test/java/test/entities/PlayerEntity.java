package test.entities;


import java.awt.event.KeyEvent;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Paths;

import endercrypt.hotline.engine.entities.GameEntity;
import endercrypt.hotline.engine.event.Event;
import endercrypt.hotline.engine.event.EventListener;
import endercrypt.hotline.engine.event.impl.keyboard.EventKeys;
import endercrypt.hotline.engine.event.impl.mouse.EventMouse;
import endercrypt.hotline.engine.event.impl.timer.EventTimer;
import endercrypt.hotline.engine.mouse.MouseButton;
import endercrypt.hotline.engine.sprite.Sprite;
import net.ddns.endercrypt.library.keyboardmanager.BindType;


public class PlayerEntity extends GameEntity
{
	private static final long serialVersionUID = -3101929237048563872L;
	
	/**
	 * 
	 */
	
	private static final double SPEED = 1.2;
	
	@EventListener(Event.CREATE)
	public void onCreate()
	{
		sprite = Sprite.of("res/player.png");
		spriteInfo.scale_x = 0.1;
		spriteInfo.scale_y = 0.1;
	}
	
	@EventListener(Event.KEY)
	@EventKeys(key = KeyEvent.VK_W)
	public void onKeyW()
	{
		motion.y -= SPEED;
	}
	
	@EventListener(Event.KEY)
	@EventKeys(key = KeyEvent.VK_S)
	public void onKeyS()
	{
		motion.y += SPEED;
	}
	
	@EventListener(Event.KEY)
	@EventKeys(key = KeyEvent.VK_A)
	public void onKeyA()
	{
		motion.x -= SPEED;
	}
	
	@EventListener(Event.KEY)
	@EventKeys(key = KeyEvent.VK_D)
	public void onKeyD()
	{
		motion.x += SPEED;
	}
	
	@EventListener(Event.KEY)
	@EventKeys(type = BindType.PRESS, key = KeyEvent.VK_U)
	public void onKeyU()
	{
		getRoom().getEntities().add(
			new PlayerEntity(),
			position.x + (Math.random() * 10),
			position.y + (Math.random() * 10));
	}
	
	@EventListener(Event.MOUSE)
	@EventMouse(global = false, button = MouseButton.RIGHT)
	public void onMouseRightClick()
	{
		getRoom().getEntities().add(new TargetEntity(), position);
	}
	
	@EventListener(Event.MOUSE)
	@EventMouse(button = MouseButton.LEFT)
	public void onGlobalMouseLeftClick()
	{
		getRoom().getEntities().add(new BulletEntity(), position);
	}
	
	@EventListener(Event.KEY)
	@EventKeys(type = BindType.PRESS, key = KeyEvent.VK_UNDERSCORE)
	public void onKeyUnderscore()
	{
		timers.forwardTimer(100, "beep");
	}
	
	@EventListener(Event.TIMER)
	@EventTimer("beep")
	public void beep()
	{
		destroy();
	}
	
	@EventListener(Event.KEY)
	@EventKeys(type = BindType.PRESS, key = KeyEvent.VK_1)
	public void onKey1() throws FileNotFoundException, IOException
	{
		getHotline().save(Paths.get("test.sav"));
	}
	
	@EventListener(Event.KEY)
	@EventKeys(type = BindType.PRESS, key = KeyEvent.VK_2)
	public void onKey2() throws FileNotFoundException, ClassNotFoundException, IOException
	{
		getHotline().load(Paths.get("test.sav"));
	}
	
	@EventListener(Event.UPDATE)
	public void onUpdate()
	{
		motion.multiplyLength(0.8);
		spriteInfo.rotation += 0.5;
		
		getRoom().getView().position().set(position);
	}
}
