package test.entities;


import endercrypt.hotline.engine.entities.GameEntity;
import endercrypt.hotline.engine.event.Event;
import endercrypt.hotline.engine.event.EventListener;
import endercrypt.hotline.engine.sprite.Sprite;
import endercrypt.library.position.Position;


public class BulletEntity extends GameEntity
{
	private static final long serialVersionUID = -4638462819239866959L;
	
	/**
	 * 
	 */
	
	@EventListener(Event.CREATE)
	public void onCreate()
	{
		sprite = Sprite.of("res/bomb.png");
		depth = 1;
		spriteInfo.scale_x = 0.5;
		spriteInfo.scale_y = 0.5;
		spriteInfo.frame_speed = 0.1;
		
		Position mousePosition = getHotline().getMouse().getPositionInRoom();
		double direction = position.calculateDirectionTo(mousePosition);
		motion.addMotion(direction, 3);
	}
	
	@EventListener(Event.UPDATE)
	protected void onStep()
	{
		spriteInfo.rotation += 1;
		spriteInfo.alpha *= 0.99;
		if (spriteInfo.alpha <= 0.1)
		{
			destroy();
		}
	}
}
