package test.entities;


import endercrypt.hotline.engine.entities.GameEntity;
import endercrypt.hotline.engine.event.Event;
import endercrypt.hotline.engine.event.EventListener;
import endercrypt.hotline.engine.event.impl.collision.EventCollision;
import endercrypt.hotline.engine.sprite.Sprite;


public class TargetEntity extends GameEntity
{
	private static final long serialVersionUID = -4638462819239866959L;
	
	/**
	 * 
	 */
	
	@EventListener(Event.CREATE)
	public void onCreate()
	{
		sprite = Sprite.of("res/science.png");
	}
	
	@EventListener(Event.COLLISION)
	@EventCollision(BulletEntity.class)
	public void onBulletCollision(BulletEntity other)
	{
		other.destroy();
	}
}
