package test;


import java.io.IOException;

import endercrypt.hotline.engine.core.HotlineGameEngine;
import endercrypt.hotline.engine.fps.MainLoop;
import endercrypt.hotline.engine.graphics.HotlineGraphics;
import endercrypt.hotline.engine.room.Room;
import endercrypt.hotline.engine.sprite.SpriteException;
import endercrypt.hotline.engine.sprite.SpriteManager;
import test.entities.PlayerEntity;
import test.entities.TargetEntity;


public class Main
{
	public static void main(String[] args) throws SpriteException, IOException
	{
		SpriteManager.registerAnimatedSprite("res/bomb.png", 64);
		
		HotlineGameEngine hotline = new HotlineGameEngine();
		hotline.getWindow().setTitle("Test game");
		hotline.getMainLoop().setFpsTarget(60);
		
		Room room = new Room()
		{
			private static final long serialVersionUID = -2373687483302804382L;
			
			/**
			 * 
			 */
			
			@Override
			public void draw(HotlineGraphics graphics)
			{
				super.draw(graphics);
				MainLoop mainLoop = getHotline().getMainLoop();
				graphics.drawString("FPS: " + mainLoop.estimateFps() + "/" + mainLoop.getFpsTarget(), 10, 15);
			}
		};
		room.getEntities().add(new TargetEntity(), 0, 0);
		room.getEntities().add(new PlayerEntity(), 10, 10);
		hotline.getRoomManager().setRoom(room);
	}
}
