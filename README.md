# HotlineGameEngine

Game Engine inspired by Game Maker

[![](https://jitpack.io/v/com.gitlab.EnderCrypt/HotlineGameEngine.svg)](https://jitpack.io/#com.gitlab.EnderCrypt/HotlineGameEngine)

Maven:
```xml
<repositories>
    <repository>
        <id>jitpack.io</id>
        <url>https://jitpack.io</url>
    </repository>
</repositories>

<dependencies>
    <dependency>
        <groupId>com.gitlab.EnderCrypt</groupId>
        <artifactId>HotlineGameEngine</artifactId>
        <version>VERSION</version>
    </dependency>
</dependencies>
```

Version should be set to a commit hash, it is recommended to use the latest one from the master branch for stability.
